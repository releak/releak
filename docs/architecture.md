# Architecture

Here is a simple review of the code architecture.
Note that the GUI only calls functions from the lib package and do not add anything appart flask and visual stuff.

```mermaid
flowchart TD
    A[lib.get_versions] -->|Get configs| AA(get github users/repo)
    AA[Get inventory] -->|Extract target repositories| AAA(Builds versions list)
    AAA --> B(get github users/repo)
    B -->|build urls| C(extract atom feed with feedparser)
    B -->|build urls| CC(extract CVE with Nist_CVE)
    C --> D(Pick selected entries)
    CC --> D
    D --> DD(Filter latest versions)
    DD --> E[Display informations]
```
