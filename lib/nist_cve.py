import logging
import requests

# TODO https://github.com/CVEProject/cvelistV5/tree/main
# TODO https://docs.opencve.io/api/cve/


class NistCVEs:
    def __init__(self):
        self.base_url = "https://services.nvd.nist.gov/rest/json/cves/2.0"
        self.search_param = "keywordSearch"
        self.logger = logging.getLogger("releak")

    def search(self, keyword):
        r = requests.get(self.base_url, params={self.search_param: keyword}, timeout=5)
        json = {}
        if r.status_code == 200:
            try:
                json = r.json()
            except requests.exceptions.JSONDecodeError as e:
                self.logger.exception(
                    "Failed to parse nist server response as json object: %s", e
                )
                self.logger.exception("See response below:\n%s", r)
        return json
