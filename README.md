# releak

Follow (leak :) releases, CVE and patch notes from the software and code you use.

The project was born when while running a Kubernetes PaaS, my team had 40+ versions to follow between kubespray products and all tools we put on post-install (traefik), with upgrades scheduled every 3 months.
At that time I did a set of ugly bash scripts to compare currents versions vs last ones, and thought that it would be nice to have this kind of tool to stay up-to-date without pain (and encourage those who don't take time to patch by reducing the work needed to follow stacks)

What we target:

* Releases on each kind of code repo, public or private: github, gitlab, gitea
* Dedicated releases websites (like kubernetes)
* Public CVE publications

What we do **not** target :

* docker image tags and other artefacts releases (helm...) as we can track them from the source repo

The philosophy:

* Simple yaml configuration, easy to import/export
* You can alternatively swap between a GUI to declare your versions (by app) and check the changes or just launch a script to get a report
* No database or always running instance, use it when you need it
* No central server or data sharing.
* Always FOSS !
* Why we moved from github => https://sfconservancy.org/GiveUpGitHub/

## Set your env

```bash
git clone https://framagit.org/releak/releak.git
python -m venv releak-venv
source releak-venv/bin/activate
pip install -r requirements.txt
```

## Just get the releases

Only github releases supported by now.
Add your repositories organized by applications and provider in `config/inventory.yaml` :

```yaml
default:
  github:
    grafana:
      grafana: v10.3.7
    traefik:
      traefik: v3.0.2
    boostorg:
      boost: boost-1.85.0

another-app:
  github:
    grafana:
      grafana: v11.0.1
    boostorg:
      boost: boost-1.85.0
    gitlabhq:
      gitlabhq: v16.11.6
```

> Only github is supported for the moment

Then launch get_versions package to get releases tags from followed repo:

```bash
python -m lib.get_versions
```

> By default log level is INFO, you can manage it through LOGLEVEL env var.

## IHM

> Test with Django => definitly overkill for our needs
> Seems easier to produce a web client rather than a PyQt interface (for sharing and prerequisites)

Launch [Flask](https://flask.palletsprojects.com/en/2.3.x/) server (test mode):

```bash
flask --app frontend/webapp run
```

Then go to `http://127.0.0.1:5000/releak` for list of versions by application

## Roadmap

**Features** :

* [X] Get basic release info from github with yaml config
* [X] Add concept of application to follow different set of releases (via dedicated yaml file)
* [ ] Get full release data: date, is breaking, link, description
* [ ] Only print delta between state set and newer stuff
* [ ] Get all versions at a time with a consolidated list of repositories
* [ ] Parallelisation of versions scrapping (as we scrape all versions each time)
* [ ] Just launch a job that report differences between upstream and current state
* [ ] Add optionnal configuration file to annotate some versions of particular product (known bug not in releases notes...)
* [ ] Add optionnal configuration file to configure repos proxy/urls
* [ ] Support SBOM ?
* [ ] Option to export result on metric format to have a graph to monitore updates and deltas ? (opentelemetry standard)
* [ ] Format output for humans and / or robots ?

**GUI**:

> If possible, keep the website static with only html/css !

* [X] Display basic release info on browser / windows
* [ ] Project logo !!!
* [ ] Better html/css to display release info
* [ ] Tab per application
* [ ] Nice homepage
* [ ] Add repositories to follow via GUI
* [ ] Be able to set versions via GUI 
* [ ] Be able to import/export via GUI 
* [ ] Flask hardenning
* [ ] Add stats tab to see how far are we from upstream and breaking changes
* [ ] Add CVE-dedicated tab
* [ ] Visual graph on welcome page with current state > target (timeline with versions as dots ?)
* [ ] Build inventory: add a "preview versions" button

**Deployment**:

* [ ] Provide docker image
* [ ] Build docker image via gitlab-CI
* [ ] Add dependency scanning (trivy)
* [ ] Provide kubernetes deployment template (on different repo)

**Supported sources**:

* [X] github
* [X] public CVE database
* [ ] gitlab
* [ ] gitea
* [ ] kubernetes releases

**Code / Refactoring**:

* [X] Clean logging
* [X] Setup linter
* [X] Sequence graph
* [X] Setup CI with linter
* [X] get rid of versions.yaml
* [ ] Structured release notes
* [ ] use POO or stay functional ?
* [ ] units tests

## Contributions

You can open issues on gitlab, submit a PR or ask to join the slack workspace.

Coding guidelines:

* We use [black](https://pypi.org/project/black/) as formatter.
* Pylint configuration embedded !
* Use dedicated branch for every change that is not documentation
* PR validated by owners/maintainers
* The flask part use get_versions functions so we can always use releak without GUI
* Use of semantic versionning for releases
* Decision changelog (data, features, architecture and design choices, integration, software...)
* Code and comments should speak by itself

Here is a review of the [code architecture](docs/architecture.md).
